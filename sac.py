# !/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  sac.py
#
#  Copyright 2018-04-13 <<autor>> <<e-mail>>
#
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
import tkinter as tk
import sys
import tkinter.messagebox as tkmsg
import gettext
import configparser
import logging
import os
from pathlib import Path
from os.path import join
import os.path as op

import appData
import aboutDlg
import copia

_ = gettext.gettext


class Mainapp(object):

    def __init__(self, **kw):

        try:
            this_file = __file__
        except NameError:
            this_file = sys.argv[0]

        this_file = op.abspath(this_file)
        if getattr(sys, 'frozen', False):
            self.application_path = os.path.dirname(sys.executable)
        else:
            self.application_path = op.dirname(this_file)
        self.homeDir = str(Path.home())
        self.logFile = join(self.homeDir, "sac/sac.log")

        # cria o diretorio se não existe- necessário para o logger
        diretorio_siggen = join(self.homeDir, "sac")

        if not os.path.exists(diretorio_siggen):
            os.makedirs(diretorio_siggen)

        # Create the Logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Create the Handler for logging data to a file
        logger_handler = logging.FileHandler(self.logFile, mode='w')
        logger_handler.setLevel(logging.INFO)

        # Create a Formatter for formatting the log messages
        logger_formatter = logging.Formatter('%(name)s - %(levelname)s \
                                             - %(message)s')

        # Add the Formatter to the Handler
        logger_handler.setFormatter(logger_formatter)

        # Add the Handler to the Logger
        self.logger.addHandler(logger_handler)
        self.logger.info(_('Logger OK'))

        inifile = join(self.homeDir, "sac/sac.ini")

        msginifile = "infile = %s" % (inifile)
        self.logger.info(msginifile)

        msgpath = "app path = %s" % (self.application_path)
        self.logger.info(msgpath)

        if not os.path.isfile(inifile):

            with open(inifile, "w") as f:
                f.write("[LastRun]\n")
                f.write("idiom = pt_BR\n")

        parser = configparser.ConfigParser()
        parser.read(inifile)
        idioma = parser.get('LastRun', 'idiom')
        self.set_language(idioma)

        msgidioma = "idioma = %s" % (idioma)
        self.logger.info(msgidioma)

        msgt = "%s  %s" % (appData.__application__, appData.__version__)

        self.root = tk.Tk()
        self.root.title(msgt)
        self.root.geometry('860x150')

        self.create_menu_bar()
        self.create_canvas_area()

    def create_canvas_area(self):
        pass

    def execute(self):
        self.root.mainloop()

    def create_menu_bar(self):
        menubar = tk.Menu(self.root)

        filemenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label=_("File"), menu=filemenu)
        filemenu.add_command(label=_("Exit"), command=self.mnuexit)

        searchmenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label=_("Search"), menu=searchmenu)
        searchmenu.add_command(label=_("By name"), command=self.copy)

        configmenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label=_("Config"), menu=configmenu)

        submenuconfig = tk.Menu(configmenu, tearoff=0)
        configmenu.add_cascade(label=_("Language"),
                               menu=submenuconfig)

        submenuconfig.add_command(label=_("Portuguese"),
                                  command=self.mnuportuguese)
        submenuconfig.add_command(label=_("English"),
                                  command=self.mnuenglish)

        helpmenu = tk.Menu(menubar, tearoff=0)
        helpmenu.add_command(label=_("About"), command=self.mnuabout)
        menubar.add_cascade(label=_("Help"), menu=helpmenu)

        self.root.config(menu=menubar)

    def mnuexit(self):
        self.root.destroy()

    def mnuabout(self):
        dlg = aboutDlg.AboutDlg(parent=self.root, title=_("About Dlg"))  # NOQA

    def change_language(self, language):
        self.idiom = language
        config = configparser.ConfigParser()

        config['LastRun'] = {}
        config['LastRun']['idiom'] = language

        arquivo_ini = join(self.homeDir, "sac/sac.ini")

        with open(arquivo_ini, 'w') as configfile:
            config.write(configfile)

        self.set_language(language)

        tkmsg.showwarning(_("Warning"),
                          _("Please restart app to modifications \
                            take effect"))

    def set_language(self, language):
        locale_dir = join(self.application_path, 'locales')

        msglocaledir = "locale dir = %s" % (locale_dir)
        self.logger.info(msglocaledir)

        if language == "pt_BR":
            gettext.bindtextdomain('pt_BR', locale_dir)
            gettext.textdomain('pt_BR')

    def mnuportuguese(self):
        self.change_language("pt_BR")

    def mnuenglish(self):
        self.change_language("en")

    def copy(self):
        dlg = copia.Copia(parent=self.root, title=_("Search and Copy"))


def main(args):
    my_app = Mainapp()
    my_app.execute()
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
