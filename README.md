# SAC - Search And Copy Utility

SAC is a Python (3.5)  software designed to help in search for files spread in large disks,
and copy them to a specific directory. File names can be given in the following formats:

* fixed name: ex  book1.pdf

* \*.pdf - will copy all pdf files

*  \*string1\*.pdf - will copy all "pdf" files with "string" in name.

It needs the following libs:

* tkinter


## Operation systems

Tested in Linux Mint 18.1, MATE interface, 64 bits

To execute:  python sac.py


## Information about versions:

* version 0.1a1   2018/06/27  Find and copy files by template name.



## Detailed instructions

Portuguese instructions can be found in https://cadernodelaboratorio.com.br

We realize that automatic translated services nowdays are terrible. Non portuguese speakers can have a hard time to understand parts of the translated text. If this is your case, you can ask us in english about anything in the blog. We will answer also in english.

Nos damos cuenta de que los textos traducidos automáticamente tienen sus limitaciones. Los que no hablan portugues pueden tener dificultades para entender parte del texto traducido. Si este es su caso, no dude en preguntarnos en español sobre cualquier informacion del blog. Responderemos también en español (or portuñol, estoy seguro que podremos llegar a entendernos).:-) ).
