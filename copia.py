# !/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  copia.py
#  Copyright 2018-05-03 tavares <tavares@tavares-Inspiron-5558>
#  1.0
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  ATENÇÂO. A versão do PILLOW DEVE SER a 3.1.2- Versões
# superiores não estão funcionando
#
# O vetor de dados correspodente ao sinal lido é um vetor numpy
# denominado self.vetor_pontos

import tksimpledialog
import tkinter as tk
import os
import logging
import sys
from pathlib import Path
from os.path import join
import gettext
from tkinter.filedialog import askdirectory
import subprocess
import shutil
import threading

_ = gettext.gettext

fim_pesquisa = 0


class Copia(tksimpledialog.Dialog):

    def __init__(self, parent, title=None):

        tksimpledialog.Dialog.__init__(self, parent, title,
                                       numero_botoes=1,
                                       btn_label=_("Close"))

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        if getattr(sys, 'frozen', False):
            # If the application is run as a bundle, the pyInstaller bootloader
            # extends the sys module by a flag frozen=True and sets the app
            # path into variable _MEIPASS'.
            application_path = sys._MEIPASS  # NOQA
        else:
            application_path = os.path.dirname(os.path.abspath(__file__))  # NOQA

        self.homeDir = str(Path.home())
        self.logFile = join(self.homeDir, "clsig1d/clsig1d.log")

        # Create the Logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Create the Handler for logging data to a file
        logger_handler = logging.FileHandler(self.logFile)
        logger_handler.setLevel(logging.INFO)

        # Create a Formatter for formatting the log messages
        logger_formatter = \
            logging.Formatter('%(name)s - %(levelname)s - %(message)s')

        # Add the Formatter to the Handler
        logger_handler.setFormatter(logger_formatter)

        # Add the Handler to the Logger
        self.logger.addHandler(logger_handler)
        self.logger.info(_('Entering module'))

        # ----------------
        box0 = tk.Frame(self, width=800, height=400)
        self.lbl0 = tk.Label(box0, text=_("Search and copy files utility"),
                             fg="blue", font=("Helvetica", 16))
        self.lbl0.pack()
        box0.pack()

        # ------------------------------

        box2 = tk.Frame(self)

        self.dir1 = tk.StringVar()
        self.dir1.set("~")
        lbl2dir1 = tk.Label(box2, text=_("Origin dir:"), padx=10, pady=10)
        lbl2dir1.grid(row=0, column=0, padx=5, pady=5)
        ent2dir1 = tk.Entry(box2, textvariable=self.dir1, width=30)
        ent2dir1.grid(row=0, column=1, padx=1, pady=5)

        self.dir2 = tk.StringVar()
        self.dir2.set("result")
        lbl2dir2 = tk.Label(box2, text=_("Destiny dir:"), padx=10, pady=10)
        lbl2dir2.grid(row=0, column=2, padx=5, pady=5)
        ent2dir2 = tk.Entry(box2, textvariable=self.dir2, width=30)
        ent2dir2.grid(row=0, column=3, padx=1, pady=5)

        self.dir3 = tk.StringVar()
        self.dir3.set("*opencv*.pdf")
        lbl2dir3 = tk.Label(box2, text=_("Search string:"), padx=10, pady=10)
        lbl2dir3.grid(row=0, column=4, padx=5, pady=5)
        ent2dir3 = tk.Entry(box2, textvariable=self.dir3, width=30)
        ent2dir3.grid(row=0, column=5, padx=1, pady=5)

        self.btnDir1 = tk.Button(box2, text=_("..."),
                                 command=self.sel_dir1)
        self.btnDir1.grid(row=1, column=1, padx=2, pady=2)

        self.btnDir2 = tk.Button(box2, text=_("..."),
                                 command=self.sel_dir2)
        self.btnDir2.grid(row=1, column=3, padx=2, pady=2)

        self.statusmsg = tk.StringVar()
        self.statusmsg.set(_("Press Search button when ready"))
        lbl2statusmsg = tk.Label(box2, text=_("Status:"), padx=10, pady=10)
        lbl2statusmsg.grid(row=1, column=4, padx=5, pady=5)
        ent2statusmsg = tk.Entry(box2, textvariable=self.statusmsg, width=30)
        ent2statusmsg.grid(row=1, column=5, padx=1, pady=5)

        # ----------------------------------------------

        box3 = tk.Frame(self, bd=2, padx=5, pady=5, relief=tk.RAISED)

        self.btnStart = tk.Button(box3, text=_("Search and copy"),
                                  command=self.start_job)
        self.btnStart.grid(row=0, column=2, padx=2, pady=2)

        box2.pack()
        box3.pack()

        return box2

    def sel_dir1(self):
        self.sourcepath = askdirectory(
                initialdir=self.dir1.get(),
                parent=self,
                title="Select Origin Directory",
                mustexist=False,
                )

        if self.sourcepath:
            self.dir1.set(self.sourcepath)

    def sel_dir2(self):
        self.destinypath = askdirectory(
                initialdir=self.dir2.get(),
                parent=self,
                title=_("Select Origin Directory"),
                mustexist=False,
                )

        if self.destinypath:
            self.dir2.set(self.destinypath)

    def start_job(self):
        self.statusmsg.set(_("Wait- Looking for files"))
        print("Procurando arquivos")

        thread1 = Mysearchthread(1, self.dir1.get(), self.dir3.get())

        thread1.start()

        print("Copiando arquivos")
        self.statusmsg.set(_("Copying files"))
        thread1.join()

        thread2 = Copiaarquivos(2, self.dir2.get())

        thread2.start()
        thread2.join()
        self.statusmsg.set(_("Files copy finished"))


class Copiaarquivos(threading.Thread):
    def __init__(self, threadid, dirdestino):
        threading.Thread.__init__(self)
        self.threadid = threadid
        self.dir_destino = dirdestino

    def run(self):
        print(_("Start file copy"))
        with open('temp.txt') as f:
            for linha in f:
                shutil.copy2(linha.rstrip('\n'), self.dir_destino)
                print(linha.rstrip('\n'))
        print(_("End file copy"))


class Mysearchthread (threading.Thread):
    def __init__(self, threadid, dirorigem, template_string):
        threading.Thread.__init__(self)
        self.threadid = threadid
        self.dir_origem = dirorigem
        self.template = template_string

    def run(self):
        print(_("Starting search"))
        param = self.dir_origem + " -type f -iname " + '"' + \
            self.template + '"'
        subprocess.call("find " + param + " > temp.txt", shell=True)
        print(_("Exiting search"))
