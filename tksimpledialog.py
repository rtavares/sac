# !/usr/bin/env python3
# -*- coding: utf-8 -*-
# Using this class is much easier than figuring out how it’s implemented;
# just create the necessary widgets in the body method
# and extract the result and carry out whatever you wish to do in the
# apply method. Here’s a simple example (we’ll take a closer look at the
# grid method in a moment).
# em 08/05/2018 foi alterado para permitir a seleção da opção dois
# botões (OKCANCEL) ou apenas um botão (OK)
# o defaul é a operação dois botoes=
# numero_botoes    1-> so OK   2 -> OKCANCEL
# em 23/06/2018:
#  acrescentado a possibilidade de alteração on line de texto do botão OK

import tkinter as tk
import gettext

_ = gettext.gettext


class Dialog(tk.Toplevel):

    def __init__(self, parent, title=None, numero_botoes=2, btn_label="OK"):

        tk.Toplevel.__init__(self, parent)
        self.transient(parent)

        self.btnQty = numero_botoes
        self.btnlabel = btn_label

        if title:
            self.title(title)

        self.parent = parent

        self.result = None

        body = tk.Frame(self)

        self.initial_focus = self.body(body)

        body.pack(padx=5, pady=5)

        self.buttonbox()

        self.grab_set()

        if not self.initial_focus:
            self.initial_focus = self

        self.protocol("WM_DELETE_WINDOW", self.cancel)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        pass

    def buttonbox(self):
        # add standard button box. override if you don't want the
        # standard buttons

        box = tk.Frame(self)

        if self.btnQty == 2:
            w = tk.Button(box, text=self.btnlabel, width=10, command=self.ok,
                          default=tk.ACTIVE)
            w.pack(side=tk.LEFT, padx=5, pady=5)
            w = tk.Button(box, text=_("Cancel"), width=10,
                          command=self.cancel)
            w.pack(side=tk.LEFT, padx=5, pady=5)

            self.bind("<Return>", self.ok)
            self.bind("<Escape>", self.cancel)

        if self.btnQty == 1:
            w = tk.Button(box, text=self.btnlabel, width=10, command=self.ok,
                          default=tk.ACTIVE)
            w.pack(side=tk.LEFT, padx=5, pady=5)
            self.bind("<Return>", self.ok)

        box.pack()

# standard button semantics

    def ok(self, event=None):

        if not self.validate():
            self.initial_focus.focus_set()  # put focus back
            return

        self.withdraw()
        self.update_idletasks()

        self.apply()

        self.result = 1000

        self.parent.focus_set()

        self.destroy()

    def cancel(self, event=None):

        self.result = 0
        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

    def validate(self):

        return 1    # override

    def apply(self):

        pass    # override
